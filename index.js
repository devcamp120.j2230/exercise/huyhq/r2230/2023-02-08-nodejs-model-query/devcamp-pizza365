const express = require("express");                                          //thư viện express

const mongoose = require("mongoose");                                       //thư viện mongoose

const drinkModel = require("./app/models/drinkModel");
const voucherModel = require("./app/models/voucherModel");
const userModel = require("./app/models/userModel");
const orderModel = require("./app/models/orderModel");

const { drinkRoute } = require("./app/routes/drinkRoute");                      //get route
const { voucherRoute } = require("./app/routes/voucherRoute");                      //get route
const { userRouter } = require("./app/routes/userRoute");                      //get route
const { orderRouter } = require("./app/routes/orderRoute");                      //get route

const path = require("path");                                                //thư viện path

const app = express();
app.use(express.json());

const port = 8000;

//kết nối với mongoDb
const nameDb = "CRUD_Pizza365";
mongoose.connect("mongodb://127.0.0.1:27017/" + nameDb, function (error) {
    if (error) throw error;
    console.log('Successfully connected to DB: ' + nameDb);
})

app.use(express.static(__dirname + "/view"));                               //middleware cho phép load ảnh trong view
app.use("/", drinkRoute);
app.use("/", voucherRoute);
app.use("/", userRouter);
app.use("/", orderRouter);

app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/view/home.pizza.html"))            //đường dẫn tới file
})


app.listen(port, () => {
    console.log("App on Port: " + port);
})