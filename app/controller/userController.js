const { default: mongoose } = require('mongoose');

const userModel = require("../models/userModel");

const getAllUser = (req, res) => {
    let sortName = req.query.sortName;
    let skipUser = req.query.skip;
    let limitUser = req.query.limit;

    let condition = {};
    userModel.find(condition)
    .sort({fullname:sortName})
    .skip(skipUser)
    .limit(limitUser)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            });
        }else{
            return res.status(200).json({
                message: `Lay du lieu thanh cong.`,
                user: data
            })
        }
    })
};

const getAUser = (req, res) => {
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `Error 400: Id khong dung dinh dang.`
        });
    }

    userModel.findById(id, (error, data)=>{
        if(error){
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            });
        }else{
            return res.status(200).json({
                message: `Lay du lieu thanh cong id: ${id}.`,
                user: data
            })
        }
    })
};

const postAUser = (req, res) => {
    var body = req.body;

    if (!body.fullname) {
        return res.status(400).json({
            message: `Error 400: Fullname phai bat buoc.`
        })
    };

    if (!body.email) {
        return res.status(400).json({
            message: `Error 400: Email phai bat buoc.`
        })
    };

    if (!body.address) {
        return res.status(400).json({
            message: `Error 400: Address phai bat buoc.`
        })
    };

    if (!body.phone) {
        return res.status(400).json({
            message: `Error 400: Phone phai bat buoc.`
        })
    };

    const newUser = new userModel({
        _id: mongoose.Types.ObjectId(),
        fullname: body.fullname,
        email: body.email,
        address: body.address,
        phone: body.phone,
    });

    userModel.create(newUser, (error, data)=>{
        if(error){
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            });
        }else{
            return res.status(201).json({
                message: `Tao moi User thanh cong.`,
                user: data
            })
        }
    })
};

const putAUser = (req, res) => {
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `Error 400: Id khong dung dinh dang.`
        });
    }

    var body = req.body;

    if (!body.fullname) {
        return res.status(400).json({
            message: `Error 400: Fullname phai bat buoc.`
        })
    };

    if (!body.email) {
        return res.status(400).json({
            message: `Error 400: Email phai bat buoc.`
        })
    };

    if (!body.address) {
        return res.status(400).json({
            message: `Error 400: Address phai bat buoc.`
        })
    };

    if (!body.phone) {
        return res.status(400).json({
            message: `Error 400: Phone phai bat buoc.`
        })
    };

    const user = new userModel({
        fullname: body.fullname,
        email: body.email,
        address: body.address,
        phone: body.phone,
    });

    userModel.findByIdAndUpdate(id, user, (error, data)=>{
        if(error){
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            });
        }else{
            return res.status(201).json({
                message: `Cap nhat thanh cong User id: ${id}.`,
                user: data
            })
        }
    })
};

const deleteAUser = (req, res) => {
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `Error 400: Id khong dung dinh dang.`
        });
    };

    userModel.findByIdAndDelete(id, (error, data)=>{
        if(error){
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            });
        }else{
            return res.status(204).json({
                message: `Xoa thanh cong User id: ${id}.`,
                user: data
            })
        }
    })
};

module.exports = {
    getAllUser,
    getAUser,
    postAUser,
    putAUser,
    deleteAUser
}