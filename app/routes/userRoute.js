const express = require('express');

const {
    getAllUser,
    getAUser,
    postAUser,
    putAUser,
    deleteAUser
} = require('../controller/userController');

const userRouter = express.Router();

userRouter.get('/user',getAllUser);

userRouter.get('/user/:id',getAUser);
userRouter.post('/user',postAUser);
userRouter.put('/user/:id',putAUser);
userRouter.delete('/user/:id',deleteAUser);

module.exports = {userRouter};
